<?php

//namespace ;
namespace App;

class Dog
{
    public $name;
    public $age;
    public $owner;
    public $breed;
    public $image;
    public $color;


    public function __construct(string $name, string $age, string $owner, string $breed,string $image,string $color)
    {
        $this->name = $name;
        $this->age = $age;
        $this->owner = $owner;
        $this->breed = $breed;
        $this->image = $image;
        $this->color = $color;
    }
    public function profile()
    {
        $info = array($this->name, $this->age,  $this->owner);
        return $info;
    }
    public function render()
    {
        $this->profile($this->name, $this->age, $this->owner);
    }
}
?>