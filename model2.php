<?php

//namespace ;
//namespace App;
interface DogArrayRepository {
    public function newDog($name, $age, $owner,$breed,$image,$color);
    public function findDog(array $query);
}
//содержит состояния
class DogArray implements DogArrayRepository 
{

	public $dogs = [];

	
	public function newDog($name, $age, $owner,$breed,$image,$color)
	{
		$this->dogs[] = new Dog($name, $age, $owner, $breed,$image,$color);
	}
    
    public function findDog(array $query)
    {
        $fd = null;
        foreach ($this->dogs as $dog)
        {
            if(($dog->name == $query['name'])&&($dog->age == $query['age'])&&($dog->owner == $query['owner'])
                &&($dog->owner == $query['owner'])&&($dog->breed == $query['breed'])
            &&($dog->color == $query['color']))
            {
                $fd = 1;
                return $dog;
                break;
            }
        }
        if($fd==null)
        {
            return 0;
        }
    }

}


class Dog
{
    public $name;
    public $age;
    public $owner;
    public $breed;
    public $image;
    public $color;


    public function __construct(string $name, string $age, string $owner, string $breed,string $image,string $color)
    {
        $this->name = $name;
        $this->age = $age;
        $this->owner = $owner;
        $this->breed = $breed;
        $this->image = $image;
        $this->color = $color;
    }
    public function profile()
    {
        $info = array($this->name, $this->age,  $this->owner);
        return $info;
    }
    public function render()
    {
        $this->profile($this->name, $this->age, $this->owner);
    }
}

class CSV {
 
    private $_csv_file = null;
 
    /**
     * @param string $csv_file  - путь до csv-файла
     */
    public function __construct($csv_file) {
        if (file_exists($csv_file)) { //Если файл существует
            $this->_csv_file = $csv_file; //Записываем путь к файлу в переменную
        }
        else { //Если файл не найден то вызываем исключение
            throw new Exception("Файл ".$csv_file." не найден"); 
        }
    }
 
    /**
     * Метод для чтения из csv-файла. Возвращает массив с данными из csv
     * @return array;
     */
    public function getCSV() {
        $handle = fopen($this->_csv_file, "r"); //Открываем csv для чтения
 
        $array_line_full = array(); //Массив будет хранить данные из csv
        //Проходим весь csv-файл, и читаем построчно. 3-ий параметр разделитель поля
        while (($line = fgetcsv($handle, 0, ";")) !== FALSE) { 
            $array_line_full[] = $line; //Записываем строчки в массив
        }
        fclose($handle); //Закрываем файл
        return $array_line_full; //Возвращаем прочтенные данные
    }
 
}
 
try {
    $csv = new CSV("myFile0.csv"); //Открываем наш csv
    /**
     * Чтение из CSV
     */
     $get_csv = $csv->getCSV();
      $da = new DogArray();
    foreach ($get_csv as $value) { //Проходим по строкам
       $da->newDog($value[0],$value[1],$value[2],$value[3],$value[4],$value[5],$value[6]);
    }

    $da->newDog('Vanya','11','Sysy','gog','','green');
    var_dump($da->dogs);
    $arr = array('name'=>'Vanya','age'=>'11','owner'=>'Sysy','breed'=>'gog','image'=>'','color'=>'green');
    var_dump($da->findDog($arr));
    $obj = $da->findDog($arr);
    var_dump($obj->profile());
    $arr = array('name'=>'Vanya','age'=>'112','owner'=>'Sysy','breed'=>'gog','image'=>'','color'=>'green');
    echo $da->findDog($arr);
}
    catch (Exception $e) { //Если csv файл не существует, выводим сообщение
    echo "Ошибка: " . $e->getMessage();
}


/**
 * Клиент: создается огромнейшая база собак со свойствами каждой: порода, изображение, цвет, имя, собственник, возраст
 */
