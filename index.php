<?php
    namespace App;
    require_once "vendor/autoload.php";
    $da = new DogArray();
    
    try {
        $csv = new CSV("myFile0.csv"); //Открываем наш csv
        /**
            * Чтение из CSV
        */
        $get_csv = $csv->getCSV();
        
        foreach ($get_csv as $value) { //Проходим по строкам
            $da->newDog($value[0],$value[1],$value[2],$value[3],$value[4],$value[5],$value[6]);
        }
        
    }
    catch (Exception $e) { //Если csv файл не существует, выводим сообщение
        echo "Ошибка: " . $e->getMessage();
    }
    
    $da->newDog('Vanya','11','Sysy','gog','','green');
    var_dump($da->dogs);
    $arr = array('name'=>'Vanya','age'=>'11','owner'=>'Sysy','breed'=>'gog','image'=>'','color'=>'green');
    var_dump($da->findDog($arr));
    $obj = $da->findDog($arr);
    var_dump($obj->profile());
    $arr = array('name'=>'Vanya','age'=>'112','owner'=>'Sysy','breed'=>'gog','image'=>'','color'=>'green');
    echo $da->findDog($arr);
?>