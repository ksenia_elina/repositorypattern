<?php
namespace App;

interface DogArrayRepository {
    public function newDog($name, $age, $owner,$breed,$image,$color);
    public function findDog(array $query);
}
?>